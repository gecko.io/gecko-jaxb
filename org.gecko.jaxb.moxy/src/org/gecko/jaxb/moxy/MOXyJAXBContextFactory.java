/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.jaxb.moxy;

import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBContextFactory;
import javax.xml.bind.JAXBException;

/**
 * 
 * @author mark
 * @since 04.08.2020
 */
public class MOXyJAXBContextFactory implements JAXBContextFactory {
	
	/* 
	 * (non-Javadoc)
	 * @see javax.xml.bind.JAXBContextFactory#createContext(java.lang.Class[], java.util.Map)
	 */
	@Override
	public JAXBContext createContext(Class<?>[] classesToBeBound, Map<String, ?> properties) throws JAXBException {
		return org.eclipse.persistence.jaxb.JAXBContextFactory.createContext(classesToBeBound, properties);
	}

	/* 
	 * (non-Javadoc)
	 * @see javax.xml.bind.JAXBContextFactory#createContext(java.lang.String, java.lang.ClassLoader, java.util.Map)
	 */
	@Override
	public JAXBContext createContext(String contextPath, ClassLoader classLoader, Map<String, ?> properties)
			throws JAXBException {
		return org.eclipse.persistence.jaxb.JAXBContextFactory.createContext(contextPath, classLoader, properties);
	}

}
