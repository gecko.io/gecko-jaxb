/**
 * Copyright (c) 2012 - 2020 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.jaxb.sun;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContextFactory;

import org.osgi.annotation.bundle.Capabilities;
import org.osgi.annotation.bundle.Capability;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.PrototypeServiceFactory;
import org.osgi.framework.ServiceRegistration;
import org.osgi.namespace.implementation.ImplementationNamespace;

/**
 * 
 * @author mark
 * @since 04.08.2020
 */
@Capabilities({
	@Capability(namespace = "osgi.service", name = "JAXBContextFactory", attribute = {Constants.OBJECTCLASS + "=javax.xml.bind.JAXBContextFactory", Constants.SERVICE_SCOPE + "=" + Constants.SCOPE_PROTOTYPE, "jaxb.provider=sun"}, uses = JAXBContextFactory.class, version = "2.3"),
	@Capability(namespace = ImplementationNamespace.IMPLEMENTATION_NAMESPACE, name = "jaxb", version = "2.3", attribute = "jaxb.provider=sun", uses = JAXBContextFactory.class)
})
public class SunActivator implements BundleActivator {
	
	private volatile Map<ServiceRegistration<?>, JAXBContextFactory> contextFactoryMap = new ConcurrentHashMap<ServiceRegistration<?>, JAXBContextFactory>();
	private ServiceRegistration<JAXBContextFactory> serviceRegistration;
	
	/* 
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		Dictionary<String, Object> properties = new Hashtable<String, Object>();
		properties.put("jaxb.provider", "sun");
		properties.put(Constants.SERVICE_SCOPE, Constants.SCOPE_PROTOTYPE);
		serviceRegistration = context.registerService(JAXBContextFactory.class, new PrototypeServiceFactory<JAXBContextFactory>() {
			/* 
			 * (non-Javadoc)
			 * @see org.osgi.framework.PrototypeServiceFactory#getService(org.osgi.framework.Bundle, org.osgi.framework.ServiceRegistration)
			 */
			@Override
			public JAXBContextFactory getService(Bundle bundle, ServiceRegistration<JAXBContextFactory> registration) {
				JAXBContextFactory factory = new SunJAXBContextFactory();
				contextFactoryMap.put(registration, factory);
				return factory;
			}
			
			/* 
			 * (non-Javadoc)
			 * @see org.osgi.framework.PrototypeServiceFactory#ungetService(org.osgi.framework.Bundle, org.osgi.framework.ServiceRegistration, java.lang.Object)
			 */
			@Override
			public void ungetService(Bundle bundle, ServiceRegistration<JAXBContextFactory> registration,
					JAXBContextFactory service) {
				contextFactoryMap.remove(registration);
			}
		}, properties);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		serviceRegistration.unregister();
	}
	
}
